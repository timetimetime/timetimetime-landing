const express = require('express');
const app = express();

// app.use((req, res, next) => {
//   console.log(req.url);
//   next();
// });

app.disable('x-powered-by');

app.use(express.static('build', { index: false }));

app.use('/user-agreement', (req, res, next) => {
  res.sendFile(`${__dirname}/build/user-agreement.html`);
});

app.use('/privacy-policy', (req, res, next) => {
  res.sendFile(`${__dirname}/build/privacy-policy.html`);
});

app.use('/', (req, res, next) => {
  res.sendFile(`${__dirname}/build/index.html`);
});

app.listen(3000, () => console.log('Landing started'));
