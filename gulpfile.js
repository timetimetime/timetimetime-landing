const gulp = require('gulp');

// HTML building
const htmlmin = require('gulp-htmlmin');
const ejs = require('gulp-ejs');
const htmlhint = require ('gulp-htmlhint');

// CSS building
const less = require('gulp-less');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');

// JS building (old)
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');

// JS building
const webpack = require('webpack-stream');
const webpackConfig = require('./webpack.prod.js');

// Image processing
const imagemin = require('gulp-imagemin');

// Live reloading
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

// Common features
const rename = require('gulp-rename');

gulp.task('watch', function() {
  gulp.watch('./source/styles/**/*', ['css']), reload;
  gulp.watch('./source/**/*.{html,ejs}', ['html']), reload;
  gulp.watch('./source/scripts/**/*', ['js']), reload;
  browserSync.init({
    server: { baseDir: './build' },
  });
});

gulp.task('html', function() {
  return gulp.src('./source/**/*.html')
    .pipe(ejs({ prefix: '/landing-static' }, { ext: '.html' }))
    .pipe(htmlhint())
    .pipe(htmlhint.reporter())
    .pipe(htmlmin({collapseWhitespace: true, removeComments: true}))
    .pipe(gulp.dest('./build'))
    .pipe(reload({stream: true}));
});

gulp.task('css', function () {
  return gulp.src('./source/styles/*.less')
    .pipe(concat('styles.less'))
    .pipe(less())
    .pipe(autoprefixer({
      browsers: ['> 0%'],
      remove: false,
    }))
    .pipe(cleanCSS())
    .pipe(rename('styles.css'))
    .pipe(gulp.dest('./build'))
    .pipe(reload({stream: true}));
});

// gulp.task('js', function(cb) {
//   return gulp.src([
//     './source/scripts/slick.js',
//     './source/scripts/main.js',
//   ])
//     .pipe(sourcemaps.init())
//     .pipe(concat('scripts.js'))
//     .pipe(uglify())
//     .pipe(sourcemaps.write('./'))
//     .pipe(gulp.dest('./build'))
//     .pipe(reload({stream: true}));
// });

gulp.task('js', function(cb) {
  return gulp.src('./source/scripts/index.js')
    .pipe(webpack(webpackConfig))
    .pipe(gulp.dest('./build'))
    .pipe(reload({stream: true}));
});

gulp.task('images', function() {
  return gulp.src('./source/images/**/*')
    // .pipe(imagemin())
    .pipe(gulp.dest('./build/images'))
});
