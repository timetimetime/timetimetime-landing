import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import CountUp from 'react-countup';

import getProjectEstimation from './project-estimation';

const _ = require('lodash');

const data = {
  subtaskCategories: [
    {
      id: 1,
      name: 'UI/UX',
      predecessors: [],
    },
    {
      id: 2,
      name: 'Back-end',
      predecessors: [],
    },
    {
      id: 3,
      name: 'Android',
      predecessors: [1, 2],
    },
    {
      id: 4,
      name: 'iOS',
      predecessors: [1, 2],
    },
  ],
  performers: [
    {
      id: 1,
      position: 'UI/UX Designer',
      salary: 70000,
    },
    {
      id: 2,
      position: 'Back-end Developer',
      salary: 95000,
    },
    {
      id: 3,
      position: 'Android Developer',
      salary: 80000,
    },
    {
      id: 4,
      position: 'iOS Developer',
      salary: 100000,
    },
  ],
  expenses: [
    {
      id: 1,
      name: 'Аренда офиса',
      cost: 65000,
    },
    {
      id: 2,
      name: 'Лицензии на ПО',
      cost: 24000,
    },
    {
      id: 3,
      name: 'Оплата серверов',
      cost: 10000,
    },
    {
      id: 4,
      name: 'Рекламные расходы',
      cost: 25000,
    },
  ],
};

const nF = new Intl.NumberFormat();

const Checkbox = ({ itemId, label, checked, onClick }) => (
  <div className="checkbox" onClick={() => onClick(itemId)}>
    <div className="checkbox__inner">
      {
        checked ?
        <div className="checkbox__status checkbox__status_active" />
        :
        <div className="checkbox__status" />
      }
      { label }
    </div>
  </div>
);

class Demo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPrice: 628670,
      previousPrice: 0,
      currentDuration: 32,
      previousDuration: 0,
      project: {
        name: 'Some mobile application',
        type: 'agile',
        insurance: 50,
        income: 20,
        taxes: 0,
        salesmanCommission: 3,
        salesmanCommissionBase: 'total',
        disabledSubtaskCategories: [],
      },
      tasks: [
        {
          id: 1,
          name: 'Новости',
          isActive: true,
          subtasks: [
            {
              id: 1,
              subtaskCategoryId: 1,
              performerId: 1,
              duration: 4,
            },
            {
              id: 2,
              subtaskCategoryId: 2,
              performerId: 2,
              duration: 2,
            },
            {
              id: 3,
              subtaskCategoryId: 3,
              performerId: 3,
              duration: 3,
            },
            {
              id: 4,
              subtaskCategoryId: 4,
              performerId: 4,
              duration: 3,
            },
          ],
        },
        {
          id: 2,
          name: 'Фотогалерея',
          isActive: true,
          subtasks: [
            {
              id: 1,
              subtaskCategoryId: 1,
              performerId: 1,
              duration: 2,
            },
            {
              id: 2,
              subtaskCategoryId: 2,
              performerId: 2,
              duration: 2,
            },
            {
              id: 3,
              subtaskCategoryId: 3,
              performerId: 3,
              duration: 3,
            },
            {
              id: 4,
              subtaskCategoryId: 4,
              performerId: 4,
              duration: 4,
            },
          ],
        },
        {
          id: 3,
          name: 'Обратная связь',
          isActive: true,
          subtasks: [
            {
              id: 1,
              subtaskCategoryId: 1,
              performerId: 1,
              duration: 1,
            },
            {
              id: 2,
              subtaskCategoryId: 2,
              performerId: 2,
              duration: 1,
            },
            {
              id: 3,
              subtaskCategoryId: 3,
              performerId: 3,
              duration: 2,
            },
            {
              id: 4,
              subtaskCategoryId: 4,
              performerId: 4,
              duration: 2,
            },
          ],
        },
      ],
    };
    this.handleCategoryToggle = this.handleCategoryToggle.bind(this);
    this.handleTaskToggle = this.handleTaskToggle.bind(this);
    this.updateResults = this.updateResults.bind(this);
  }
  handleCategoryToggle(categoryId) {
    const { disabledSubtaskCategories } = this.state.project;
    let updatedListOfDisabled = null;
    if (disabledSubtaskCategories.includes(categoryId)) {
      updatedListOfDisabled =
        disabledSubtaskCategories.filter(el => {
          return el !== categoryId;
        });
    } else {
      updatedListOfDisabled =
        [...disabledSubtaskCategories, categoryId];
    }
    const updatedProject = Object.assign({}, this.state.project, {
      disabledSubtaskCategories: updatedListOfDisabled,
    });
    this.setState({
      project: updatedProject,
    }, this.updateResults);
  }
  handleTaskToggle(taskId) {
    const { tasks } = this.state;
    const update = tasks.map(el => {
      let result = null;
      if (el.id === taskId) {
        result = Object.assign({}, el, { isActive: !el.isActive });
      } else {
        result = el;
      }
      return result;
    });
    this.setState({ tasks: update }, this.updateResults);
  }
  updateResults() {
    const source = Object.assign({}, data, {
      project: this.state.project,
      tasks: this.state.tasks,
    });
    const estimation = getProjectEstimation(_.cloneDeep(source));
    this.setState({
      currentPrice: estimation.cost.total,
      previousPrice: this.state.currentPrice,
      currentDuration: estimation.duration.calendarDays,
      previousDuration: this.state.currentDuration,
    });
  }
  render() {
    const {
      currentPrice,
      previousPrice,
      currentDuration,
      previousDuration,
    } = this.state;
    return (
      <div className="row">
        <div className="col-xs-12 col-sm-4">
          <h3 className="about__subtitle">
            Итого:
          </h3>
          <p className="about__calculation">
            Стоимость: <b>{
              <CountUp
                duration={0.8}
                start={previousPrice}
                end={currentPrice}
                formattingFn={nF.format}
              />
            } руб.</b>
          </p>
          <p className="about__calculation">
            Срок: <b>{
              <CountUp
                duration={0.4}
                start={previousDuration}
                end={currentDuration}
                formattingFn={nF.format}
              />
            } дней</b>
          </p>
        </div>
        <div className="col-xs-6 col-sm-4">
          <h3 className="about__subtitle">
            Задачи:
          </h3>
          {
            this.state.tasks.map(el =>
              <Checkbox
                key={el.id}
                itemId={el.id}
                label={el.name}
                checked={el.isActive}
                onClick={this.handleTaskToggle}
              />
            )
          }
        </div>
        <div className="col-xs-6 col-sm-4">
          <h3 className="about__subtitle">
            Виды работ:
          </h3>
          {
            data.subtaskCategories.map(el =>
              <Checkbox
                key={el.id}
                itemId={el.id}
                label={el.name}
                checked={!this.state.project.disabledSubtaskCategories.includes(el.id)}
                onClick={this.handleCategoryToggle}
              />
            )
          }
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <Demo />,
  document.getElementById('demo')
);
